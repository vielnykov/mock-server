# Mock Server with Proxy #

This small NodeJS script is a middleware between your client-side application and API server. You can define mock API methods and the rest of requests will be transferred to the existing server.

### Installation ###

* Check if you have installed ```NodeJS``` on your computer. Download link: https://nodejs.org/en/download/

* Clone the project ```git clone https://bitbucket.org/vielnykov/mock-server && cd mock-server```

* Install all dependencies ```npm i```

* Create ```conf-local.json``` file for your local settings with following sample content:
```
#!json
{
  "realServerURL": "http://real-server-url.example/",
  "defaultHeaders": {
    "Cache-Control": "private",
    "Content-Type": "application/json; charset=utf-8",
    "Strict-Transport-Security": "max-age=15552000"
  },
  "mockRoutes": [
  ],
  "mockFailureRoutes": [
  ]
}
```
You can overwrite main settings in ```conf.json```

### Create mock method ###

* Create ```methods/api/v1/user.json``` file with needed json content

* Create ```methods/api/v1/user-failure.json``` file with failure json content if needed.

* Put path in ```conf-local.json``` configuration file in ```mockRoutes ``` part:
```
"mockRoutes": [
  "/api/v1/user"
]
```

* Put path in ```conf-local.json``` configuration file in ```mockFailureRoutes ``` part if you need failure response: 
```
"mockFailureRoutes": [
  "/api/v1/user"
]
```


### Usage ###

* Start script: ```npm start```
* Try to request in your browser following URL: ```http://localhost:3000/api/v1/user```
* All not mentioned in ```conf-local.json``` routes will be transferred to the URL, saved in settings as ```realServerUrl```