process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
let url = require('url'),
    proxy = require('proxy-middleware'),
    connect = require('connect')(),
    express = require('express')(),
    bodyParser = require('body-parser'),
    fs = require('fs'),
    conf = parseConfFiles();

function parseConfFiles() {
  let conf      = JSON.parse(fs.readFileSync(__dirname + '/conf.json', 'utf8')),
      confLocal = JSON.parse(fs.readFileSync(__dirname + '/conf-local.json', 'utf8'));

  return Object.assign(conf, confLocal);
}

function getFilePath(route) {
  let _route = '/' + route.replace(new RegExp(/(^\/|\/$)/, 'g'), ''),
      isFailureResponse = conf.mockFailureRoutes.indexOf(_route) >= 0,
      fileNotFoundFilePath = __dirname + '/defaults/fileNotFound.json',
      fileName = _route + (isFailureResponse ? '-failure.json' : '.json'),
      filePath = __dirname + '/methods' + fileName;

  return fs.existsSync(filePath) ? filePath : fileNotFoundFilePath;
}

express.use(bodyParser.json());

express.use(/^(.+)$/, function(req, res) {
  let route = req.params[0],
      filePath = getFilePath(route);
  for (headerName in conf.defaultHeaders)
    res.set(headerName, conf.defaultHeaders[headerName]);
  let logData = route + "\n\n" + JSON.stringify(req.body);
  fs.writeFile(__dirname + '/log/' + new Date().getTime() + '.json', logData, function() {});
  console.log('Mocking route: ' + route);
  res.removeHeader('Connection');
  res.removeHeader('X-Powered-By');
  res.sendFile(filePath);
});

express.listen(conf.mockServerPort, function() { console.log('[Mock Server] Listening localhost:' + conf.mockServerPort); });

for(i in conf.mockRoutes) {
  let route = conf.mockRoutes[i];
  connect.use(route, proxy('http://localhost:' + conf.mockServerPort + route))
}

connect.use('/', function(req) {
  console.log('Proxing route: ' + req.url);
  let options = url.parse(conf.realServerURL);
  return proxy(options).apply(null, arguments);
});

connect.listen(conf.port, function() { console.log('[Proxy] Listening localhost:' + conf.port); });
